from selenium.webdriver import ActionChains


class PikabuNavigator:
    def __init__(self, driver):
        self.driver = driver

    def _press(self, key):
        ActionChains(self.driver).send_keys(key).pause(0.5).perform()

    def next_post(self):
        self._press("d")

    def previous_post(self):
        self._press("a")

    def to_post_head(self):
        self._press("x")

    def down(self):
        self._press("c")

    def up(self):
        self._press("z")
