import pytest

from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys

import urllib.parse

from tests.utils import PikabuNavigator


@pytest.fixture(name="base_url", scope="session")
def base_url_fixture():
    return "https://pikabu.ru"


@pytest.mark.parametrize(
    "vote_element",
    [
        "rating-up",
        "rating-down",
    ],
)
def test_cant_vote_without_login(driver, base_url, vote_element):
    driver.get(base_url)
    first_vote_element = driver.find_element_by_class_name(f"story__{vote_element}")

    (
        ActionChains(driver)
        .move_to_element(first_vote_element)
        .click(first_vote_element)
        .perform()
    )

    auth_popup = driver.find_element_by_class_name("auth-modal")
    assert auth_popup is not None


def get_y_offset(driver):
    return driver.execute_script("return window.pageYOffset;")


def is_vertically_visible(driver, element, minimum_visible_px=200):
    driver_y_offset = get_y_offset(driver)

    return (
        driver_y_offset
        <= element.location["y"]
        <= element.location["y"] + minimum_visible_px
        <= driver_y_offset + driver.get_window_size()["height"]
    )


def test_keyboard_navigation_full(driver, base_url):
    driver.set_window_size(
        width=800, height=600
    )  # the test is highly sensitive to the vertical size
    nav = PikabuNavigator(driver)
    single_scroll_offset_px = 100

    driver.get(base_url)
    assert get_y_offset(driver) == 0

    stories = driver.find_elements_by_class_name("story__main")
    assert len(stories) > 0
    assert not is_vertically_visible(driver, stories[0])

    nav.next_post()
    assert is_vertically_visible(driver, stories[0])
    assert not is_vertically_visible(driver, stories[1])

    nav.next_post()
    assert not is_vertically_visible(driver, stories[0])
    assert is_vertically_visible(driver, stories[1])

    nav.previous_post()
    assert is_vertically_visible(driver, stories[0])
    assert not is_vertically_visible(driver, stories[1])

    first_story_position = get_y_offset(driver)
    nav.down()
    assert get_y_offset(driver) == first_story_position + single_scroll_offset_px

    nav.down()
    nav.up()
    assert get_y_offset(driver) == first_story_position + single_scroll_offset_px
    assert not is_vertically_visible(driver, stories[0])

    nav.to_post_head()
    assert is_vertically_visible(driver, stories[0])


@pytest.fixture(name="tag", scope="session")
def tag_fixture():
    return "Деньги"


def test_search_by_tag(driver, tag, base_url):
    url = f"{base_url}/search"
    driver.get(url)

    tags_filter = driver.find_element_by_class_name("stories-search__tags-group")
    expand_tags_filter_btn = tags_filter.find_element_by_class_name(
        "form-group__button"
    )
    tag_input = tags_filter.find_element_by_class_name("input__input")
    search_btn = driver.find_element_by_id("search_stories_button")

    (
        ActionChains(driver)
        .move_to_element(tags_filter)
        .click(expand_tags_filter_btn)
        .click(tag_input)
        .send_keys(tag)
        .pause(1)
        .send_keys(Keys.ENTER)
        .pause(0.5)
        .click(search_btn)
        .pause(2)  # give page some time to fully load
        .perform()
    )

    assert driver.current_url == f"{base_url}/tag/{urllib.parse.quote_plus(tag)}"
    stories_tags = [
        [
            tag_el.get_attribute("data-tag")
            for tag_el in story_tags_el.find_elements_by_class_name("tags__tag")
            if tag_el.tag_name != "button"
        ]
        for story_tags_el in driver.find_elements_by_class_name("story__tags")
    ]

    assert len(stories_tags) > 0
    assert all(tag in story_tags for story_tags in stories_tags)
