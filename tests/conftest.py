import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


def pytest_addoption(parser):
    parser.addoption("--remote", help="is running in CI?", action="store_true")


@pytest.fixture(scope="session")
def is_remote(request):
    return request.config.getoption("--remote")


@pytest.fixture
def chrome_options(is_remote):
    options = Options()
    if is_remote:
        options.add_argument("--no-sandbox")
        options.add_argument("--disable-dev-shm-usage")
    return options


@pytest.fixture()
def driver(is_remote, chrome_options):
    if is_remote:
        driver = webdriver.Remote(
            command_executor="http://selenium__standalone-chrome:4444/wd/hub",
            options=chrome_options,
        )
    else:
        driver = webdriver.Chrome(
            options=chrome_options,
        )

    yield driver

    driver.quit()
